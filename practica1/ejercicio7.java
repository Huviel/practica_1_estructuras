/*Fernando Mamani Veliz
 *Realizar un programa que cuente el número de cifras de un número entero positivo
(hay que controlarlo) pedido por teclado. Crea un método que realice 
*/
package practica1;
import javax.swing.JOptionPane;
public class ejercicio7 {
    public static void main(String[] args) {
        int result=0;
        String typeText="";
        typeText=JOptionPane.showInputDialog("Para calcular la cantidad de cifras introdusca el número a calcular");
        int a=Integer.parseInt(typeText);
        result=cifras(a);
        JOptionPane.showMessageDialog(null,"el número"+" "+a+" "+"tiene"+" "+result+" "+"cifras");
    }
    public static int cifras(int a){
        int count=0;
        while(a>0){
            a=a/10;
            count++;
        }
        return count;
    }
}
