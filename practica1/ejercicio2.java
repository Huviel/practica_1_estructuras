/*Fernando Mamani Veliz
 * Realizar un programa que te realice el promedio de las notas de un alumno, para
ello el programa te va a tener que solicitar el nombre del alumno y las notas de las
3 evaluaciones.
 */
package practica1;
import javax.swing.JOptionPane;
public class ejercicio2 {
    public static void main(String[] args) {
        double result=0;
        String typetext="";
        String estudiante="";
        estudiante=JOptionPane.showInputDialog("Introduce Nombre completo del estudiante");
        typetext=JOptionPane.showInputDialog("introdusca la primera nota");
        double a=Integer.parseInt(typetext);
        typetext=JOptionPane.showInputDialog("introdusca la segunda nota");
        double b=Integer.parseInt(typetext);
        typetext=JOptionPane.showInputDialog("introdusca la tercera nota");
        double c=Integer.parseInt(typetext);
        result=promedio(a,b,c);
        JOptionPane.showMessageDialog(null,"El promedio del estudiante"+" "+estudiante+" "+"es"+" " +result);
    }
   public static double promedio(double a,double b,double c){
       double resultado=(a+b+c)/3;
       return resultado;
   }
    
}
