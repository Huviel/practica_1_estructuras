/*Fernando Mamani Veliz
 * Realizar un programa que nos convierta un número en base decimal a binario. Esto lo realizara un método al que le pasaremos el numero como parámetro, devolverá un String con el numero convertido a binario. Para convertir un numero
decimal a binario, debemos dividir entre 2 el numero y el resultado de esa división
se divide entre 2 de nuevo hasta que no se pueda dividir mas, el resto que
obtengamos de cada división formara el numero binario, de abajo a arriba.*/
package practica1;
import javax.swing.JOptionPane;
public class ejercicio6 {
    public static void main(String[] args) {
        String binar="";
        String typeText="";
        typeText=JOptionPane.showInputDialog("Introdusca el numero decimal para convertir a binario");
        int a=Integer.parseInt(typeText);
        binar=binario(a);
        JOptionPane.showMessageDialog(null,"el numero decimal"+" "+a+" "+"tiene el siguiente numero binario:"+" "+binar);
    }
    public static String binario (int a){
        int b=0;
        String c="";
        while(a>0){
            b=a%2;
            c=b+c;
            a=a/2;
        }
        return c; 
    }
}
