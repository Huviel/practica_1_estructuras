/*Fernando Mamani Veliz
 * Realizar un programa que nos convierta una cantidad de Bolivianos introducida por
teclado a otra moneda, estas pueden ser a dolares, euros o libras. El método tendrá
como parámetros, la cantidad de bolivianos y la moneda a pasar que sera una
cadena, este no devolverá ningún valor, mostrara un mensaje indicando el cambio.*/
package practica1;
import javax.swing.JOptionPane;
import java.text.DecimalFormat;
public class ejercicio8 {
    public static void main(String[] args) {
        String typetext=JOptionPane.showInputDialog("introdusca la cantidad en Bs.");
        double bol=Integer.parseInt(typetext);
        String money=JOptionPane.showInputDialog("¿a qué moneda desea cambiar?");
        JOptionPane.showMessageDialog(null, cambio(bol,money));
    }
    public static String cambio (double bol, String money){
    double res=0;
 
   
        switch (money){
        case "libras":
            res=bol*0.11552;
            break;
        case "dolar":
            res=bol*0.14451;
            break;
        case "euros":
            res=bol*0.12930;
            break;
        default:
           JOptionPane.showMessageDialog(null, "error");
        }
        DecimalFormat df = new DecimalFormat("#.00");
         return bol+ " bolivianos en " +money+ " son " +df.format(res);
    }
}
