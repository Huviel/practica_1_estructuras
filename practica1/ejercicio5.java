/*Fernando Mamani Veliz
*Realizar un programa que lea dos números por teclado y muestre el resultado de la
división del primer número por el segundo. Se debe comprobar que el divisor no
puede ser cero.*/
package practica1;
import javax.swing.JOptionPane;
public class ejercicio5 {
  public static void main(String[] args) {
      double resul=0;
      String typetext="";
      typetext=JOptionPane.showInputDialog("introdusca el número del dividendo");
      double a=Integer.parseInt(typetext);
      typetext=JOptionPane.showInputDialog("introdusca el número del divisor");
      double b=Integer.parseInt(typetext);
      resul=division(a,b);
      JOptionPane.showMessageDialog(null,"el resultado de su division es:"+" "+ resul);
      
  }
  public static double division(double a,double b){
      if(b==0){
      JOptionPane.showMessageDialog(null,"error intenta cambiar el divisor a un numero distinto de cero");
      }
      double resultado = a/b; 
      return resultado;
  }
}
