/*Fernando Mamani Veliz
 * Realizar un programa Java que calcule el área de un triángulo en función de las
longitudes de sus lados (a, b, c), según la siguiente fórmula: Area =
RaizCuadrada(p*(p-a)*(p-b)*(p-c)) donde p = (a+b+c)/2 Para calcular la raíz
cuadrada se utiliza el método Math.sqrt()
 */
package practica1;
import javax.swing.JOptionPane;
import java.text.DecimalFormat;
public class ejercicio9 {
    public static void main(String[] args) {
        double result=0;
        String typetext="";
        typetext=JOptionPane.showInputDialog("Introdusca el valor del primer lado del triángulo");
        int a=Integer.parseInt(typetext);
        typetext=JOptionPane.showInputDialog("Introdusca el valor del segundo lado del triángulo");
        int b=Integer.parseInt(typetext);
        typetext=JOptionPane.showInputDialog("Introdusca el valor de base del triángulo");
        int c=Integer.parseInt(typetext);
        result=area(a,b,c);
        DecimalFormat df=new DecimalFormat("#.00");
        JOptionPane.showMessageDialog(null,"El area del triangulo es:"+" "+df.format(result));
    }
   public static double area(double a,double b,double c){
       double area=0;
       double p = (a+b+c)/2;
       area=Math.sqrt(p*(p-a)*(p-b)*(p-c));
       return area;
   } 
}
