/*Fernando Mamani Veliz
 * Realizar un programa que calcule el sueldo de un trabajador, el programa va a
solicitar el numero de horas que has trabajado en un mes, las horas se pagan a
10Bs. Ejercicio
 */
package practica1;
import javax.swing.JOptionPane;
public class ejercicio3 {
    public static void main(String[] args) {
        int result=0;
        String typetext="";
        typetext=JOptionPane.showInputDialog("Introdusca el numero de horas que a trabajado durante el mes");
        int h=Integer.parseInt(typetext);
        result=sueldo(h);
        JOptionPane.showMessageDialog(null,"el sueldo que debe cobrar es de"+" "+result+" "+"Bs.");
    }
    public static int sueldo(int h){
        int resultado=h*10;
        return resultado;
    }
}
